const demo1 = {
    "list": [{
            "type": "text",
            "name": "",
            "icon": "el-icon-share",
            "options": {
                "nameDisabled": true,
                "defaultValue": "疫情信息统计收集表",
                "position": "center",
                "lineHeight": 25,
                "size": 22,               
                "color": "#0570D5",
                "remoteFunc": "func_1609996420000_18565",
                "indent": 0,
                "leftPadding": 0,
                "weight": "",
                "style": ""
            },
            "key": "1609996420000_18565",
            "model": "text_1609996420000_18565",
            "rules": []
        },
        {
            "type": "radio",
            "name": "Q1. 请问您是本人上报还是替别人上报？（单选，必答）",
            "icon": "el-icon-share",
            "options": {
                "inline": "block",
                "valueData": [{
                        "value": "本人，上报自己的信息",
                        "label": "本人，上报自己的信息"
                    },
                    {
                        "value": "别人，代填他人的信息",
                        "label": "别人，代填他人的信息"
                    }
                ],
                "defaultValue": "",
                "required": true,
                "remoteFunc": "func_1609901988000_16912",
                "isTag": true
            },
            "key": "1609901988000_16912",
            "model": "radio_1609901988000_16912",
            "rules": []
        },
        {
            "type": "input",
            "name": "Q2. 代填人姓名（本人上报无需填写）",
            "icon": "el-icon-share",
            "options": {
                "width": "100%",
                "placeholder": "请输入代填人姓名（本人上报无需填写）",
                "defaultValue": "",
                "required": false,
                "clearable": false,
                "maxlength": "",
                "remoteFunc": "func_1609902043000_85931"
            },
            "key": "1609902043000_85931",
            "model": "input_1609902043000_85931",
            "rules": []
        },
        {
            "type": "input",
            "name": "Q3. 代填人手机号（本人上报无需填写）",
            "icon": "el-icon-share",
            "options": {
                "width": "100%",
                "placeholder": "请输入代填人手机号（本人上报无需填写）",
                "defaultValue": "",
                "required": false,
                "clearable": false,
                "maxlength": "",
                "remoteFunc": "func_1609902071000_2223"
            },
            "key": "1609902071000_2223",
            "model": "input_1609902071000_2223",
            "rules": []
        },
        {
            "type": "input",
            "name": "Q4. 上报人姓名（必答）",
            "icon": "el-icon-share",
            "options": {
                "width": "100%",
                "placeholder": "请输入上报人姓名",
                "defaultValue": "",
                "required": true,
                "clearable": false,
                "maxlength": "",
                "remoteFunc": "func_1609902101000_35503"
            },
            "key": "1609902101000_35503",
            "model": "input_1609902101000_35503",
            "rules": []
        },
        {
            "type": "input",
            "name": "Q5. 上报人手机（必答）",
            "icon": "el-icon-share",
            "options": {
                "width": "100%",
                "placeholder": "请输入上报人手机号",
                "defaultValue": "",
                "required": true,
                "clearable": false,
                "maxlength": "",
                "remoteFunc": "func_1609902139000_51204"
            },
            "key": "1609902139000_51204",
            "model": "input_1609902139000_51204",
            "rules": []
        },
        {
            "type": "radio",
            "name": "Q6. 上报人性别（单选，必答）",
            "icon": "el-icon-share",
            "options": {
                "inline": "block",
                "valueData": [{
                        "value": "男",
                        "label": "男"
                    },
                    {
                        "value": "女",
                        "label": "女"
                    }
                ],
                "defaultValue": "",
                "required": true,
                "remoteFunc": "func_1609902191000_52911",
                "isTag": true
            },
            "key": "1609902191000_52911",
            "model": "radio_1609902191000_52911",
            "rules": []
        },
        {
            "type": "radio",
            "name": "Q7. 上报人年龄（单选，必答）",
            "icon": "el-icon-share",
            "options": {
                "inline": "block",
                "valueData": [{
                        "value": "0-20岁",
                        "label": "0-20岁"
                    },
                    {
                        "value": "21-30岁",
                        "label": "21-30岁"
                    },
                    {
                        "value": "31-40岁",
                        "label": "31-40岁"
                    },
                    {
                        "value": "41-50岁",
                        "label": "41-50岁"
                    },
                    {
                        "value": "51及以上",
                        "label": "51及以上"
                    }
                ],
                "defaultValue": "",
                "required": true,
                "remoteFunc": "func_1609902223000_55489",
                "isTag": true
            },
            "key": "1609902223000_55489",
            "model": "radio_1609902223000_55489",
            "rules": []
        },
        {
            "type": "textarea",
            "name": "Q8. 上报人详细住址（请精确到门牌号，必答）",
            "icon": "el-icon-share",
            "options": {
                "width": "100%",
                "rows": 2,
                "placeholder": "请输入上报人详细住址",
                "defaultValue": "",
                "required": true,
                "maxlength": "",
                "remoteFunc": "func_1609902298000_43510"
            },
            "key": "1609902298000_43510",
            "model": "textarea_1609902298000_43510",
            "rules": []
        },
        {
            "type": "checkbox",
            "name": "Q9. 上报人是否存在以下症状？（多选，必答）",
            "icon": "el-icon-share",
            "options": {
                "inline": "block",
                "valueData": [{
                        "value": "发热",
                        "label": "发热"
                    },
                    {
                        "value": "干咳",
                        "label": "干咳"
                    },
                    {
                        "value": "乏力",
                        "label": "乏力"
                    },
                    {
                        "value": "咳痰",
                        "label": "咳痰"
                    },
                    {
                        "value": "鼻塞",
                        "label": "鼻塞"
                    },
                    {
                        "value": "流涕",
                        "label": "流涕"
                    },
                    {
                        "value": "呼吸困难",
                        "label": "呼吸困难"
                    },
                    {
                        "value": "头疼",
                        "label": "头疼"
                    },
                    {
                        "value": "胸闷",
                        "label": "胸闷"
                    },
                    {
                        "value": "呕吐",
                        "label": "呕吐"
                    },
                    {
                        "value": "腹泻",
                        "label": "腹泻"
                    },
                    {
                        "value": "以上都没有",
                        "label": "以上都没有"
                    },
                    {
                        "value": "其他症状（请说明）",
                        "label": "其他症状（请说明）"
                    }
                ],
                "defaultValue": [],
                "required": true,
                "remoteFunc": "func_1609902343000_96556",
                "isTag": true
            },
            "key": "1609902343000_96556",
            "model": "checkbox_1609902343000_96556",
            "rules": []
        },
        {
            "type": "textarea",
            "name": "其他症状（请说明）",
            "icon": "el-icon-share",
            "options": {
                "width": "100%",
                "rows": 2,
                "placeholder": "请输入其他症状",
                "defaultValue": "",
                "required": false,
                "maxlength": "",
                "remoteFunc": "func_1609902457000_65860"
            },
            "key": "1609902457000_65860",
            "model": "textarea_1609902457000_65860",
            "rules": []
        },
        {
            "type": "checkbox",
            "name": "Q10. 上报人近两周内，是否有以下情况？（多选，必答）",
            "icon": "el-icon-share",
            "options": {
                "inline": "block",
                "valueData": [{
                        "value": "接触过来自湖北的人士或亲友",
                        "label": "接触过来自湖北的人士或亲友"
                    },
                    {
                        "value": "接触过有发热、干咳、乏力、呼吸困难等症状的人士或亲友",
                        "label": "接触过有发热、干咳、乏力、呼吸困难等症状的人士或亲友"
                    },
                    {
                        "value": "自己有发热、干咳、乏力、呼吸困难等症状",
                        "label": "自己有发热、干咳、乏力、呼吸困难等症状"
                    },
                    {
                        "value": "以上情况均无",
                        "label": "以上情况均无"
                    }
                ],
                "defaultValue": [],
                "required": true,
                "remoteFunc": "func_1609902486000_53208",
                "isTag": true
            },
            "key": "1609902486000_53208",
            "model": "checkbox_1609902486000_53208",
            "rules": []
        },
        {
            "type": "textarea",
            "name": "Q11. 上报者当前所居住的位置（地理位置信息）",
            "icon": "el-icon-share",
            "options": {
                "width": "100%",
                "rows": 2,
                "placeholder": "请输入上报者当前所居住的位置",
                "defaultValue": "",
                "required": false,
                "maxlength": "",
                "remoteFunc": "func_1609902540000_88819"
            },
            "key": "1609902540000_88819",
            "model": "textarea_1609902540000_88819",
            "rules": []
        }
    ],
    "config": {
        "labelWidth": 100,
        "labelPosition": "top",
        "size": "mini"
    }
}
const demo2 = {
    "list": [{
            "type": "radio",
            "name": "请问您的性别是",
            "icon": "el-icon-share",
            "options": {
                "inline": "inline-block",
                "valueData": [{
                        "value": "男",
                        "label": "男"
                    },
                    {
                        "value": "女",
                        "label": "女"
                    }
                ],
                "defaultValue": "",
                "required": true,
                "remoteFunc": "func_1609918480000_457",
                "isTag": true
            },
            "key": "1609918480000_457",
            "model": "radio_1609918480000_457",
            "rules": []
        },
        {
            "type": "radio",
            "name": "您所在的年级",
            "icon": "el-icon-share",
            "options": {
                "inline": "inline-block",
                "valueData": [{
                        "value": "大一",
                        "label": "大一"
                    },
                    {
                        "value": "大二",
                        "label": "大二"
                    },
                    {
                        "value": "大三",
                        "label": "大三"
                    }
                ],
                "defaultValue": "",
                "required": true,
                "remoteFunc": "func_1609918527000_75187",
                "isTag": true,
                "border": false
            },
            "key": "1609918527000_75187",
            "model": "radio_1609918527000_75187",
            "rules": []
        },
        {
            "type": "input",
            "name": "你的学校所在省份是",
            "icon": "el-icon-share",
            "options": {
                "width": "100%",
                "placeholder": "请输入你的学校所在省份",
                "defaultValue": "",
                "required": true,
                "clearable": false,
                "maxlength": "",
                "remoteFunc": "func_1609918575000_77155"
            },
            "key": "1609918575000_77155",
            "model": "input_1609918575000_77155",
            "rules": []
        },
        {
            "type": "radio",
            "name": "你的学校所在环境是",
            "icon": "el-icon-share",
            "options": {
                "inline": "inline-block",
                "valueData": [{
                        "value": "市区",
                        "label": "市区"
                    },
                    {
                        "value": "郊区",
                        "label": "郊区"
                    }
                ],
                "defaultValue": "",
                "required": true,
                "remoteFunc": "func_1609918643000_4223",
                "isTag": true
            },
            "key": "1609918643000_4223",
            "model": "radio_1609918643000_4223",
            "rules": []
        },
        {
            "type": "radio",
            "name": "您的家庭月收入",
            "icon": "el-icon-share",
            "options": {
                "inline": "inline-block",
                "valueData": [{
                        "value": "10000万以下",
                        "label": "10000万以下"
                    },
                    {
                        "value": "10000~15000元",
                        "label": "10000~15000元"
                    },
                    {
                        "value": "15000~20000元",
                        "label": "15000~20000元"
                    },
                    {
                        "value": "20000元及以上",
                        "label": "20000元及以上"
                    }
                ],
                "defaultValue": "",
                "required": true,
                "remoteFunc": "func_1609918675000_86598",
                "isTag": true
            },
            "key": "1609918675000_86598",
            "model": "radio_1609918675000_86598",
            "rules": []
        },
        {
            "type": "radio",
            "name": "你在校期间的月平均消费是",
            "icon": "el-icon-share",
            "options": {
                "inline": "inline-block",
                "valueData": [{
                        "value": "300元以下",
                        "label": "300元以下"
                    },
                    {
                        "value": "300-600元",
                        "label": "300-600元"
                    },
                    {
                        "value": "600-1000元",
                        "label": "600-1000元"
                    },
                    {
                        "value": "1000-1500元",
                        "label": "1000-1500元"
                    },
                    {
                        "value": "1500以上",
                        "label": "1500以上"
                    }
                ],
                "defaultValue": "",
                "required": true,
                "remoteFunc": "func_1609918742000_73989",
                "isTag": true
            },
            "key": "1609918742000_73989",
            "model": "radio_1609918742000_73989",
            "rules": []
        },
        {
            "type": "radio",
            "name": "每月用于校内食堂的费用是",
            "icon": "el-icon-share",
            "options": {
                "inline": "inline-block",
                "valueData": [{
                        "value": "200元以下",
                        "label": "200元以下"
                    },
                    {
                        "value": "200-400元",
                        "label": "200-400元"
                    },
                    {
                        "value": "400-600元",
                        "label": "400-600元"
                    },
                    {
                        "value": "600元以上",
                        "label": "600元以上"
                    }
                ],
                "defaultValue": "",
                "required": true,
                "remoteFunc": "func_1609918801000_70487",
                "isTag": true
            },
            "key": "1609918801000_70487",
            "model": "radio_1609918801000_70487",
            "rules": []
        },
        {
            "type": "radio",
            "name": "每月电话费是",
            "icon": "el-icon-share",
            "options": {
                "inline": "inline-block",
                "valueData": [{
                        "value": "30元以下",
                        "label": "30元以下"
                    },
                    {
                        "value": "30-60元",
                        "label": "30-60元"
                    },
                    {
                        "value": "60-100元",
                        "label": "60-100元"
                    },
                    {
                        "value": "100元以上",
                        "label": "100元以上"
                    },
                    {
                        "value": "新选项297",
                        "label": "新选项297"
                    }
                ],
                "defaultValue": "",
                "required": true,
                "remoteFunc": "func_1609918871000_96716",
                "isTag": true
            },
            "key": "1609918871000_96716",
            "model": "radio_1609918871000_96716",
            "rules": []
        },
        {
            "type": "radio",
            "name": "平均每月购置衣服的费用是",
            "icon": "el-icon-share",
            "options": {
                "inline": "inline-block",
                "valueData": [{
                        "value": "50元以下",
                        "label": "50元以下"
                    },
                    {
                        "value": "50-150元",
                        "label": "50-150元"
                    },
                    {
                        "value": "150-300元",
                        "label": "150-300元"
                    },
                    {
                        "value": "300元以上",
                        "label": "300元以上"
                    }
                ],
                "defaultValue": "",
                "required": true,
                "remoteFunc": "func_1609918992000_21160",
                "isTag": true
            },
            "key": "1609918992000_21160",
            "model": "radio_1609918992000_21160",
            "rules": []
        },
        {
            "type": "radio",
            "name": "平均每月购买日常用品的费用是",
            "icon": "el-icon-share",
            "options": {
                "inline": "inline-block",
                "valueData": [{
                        "value": "10元以下",
                        "label": "10元以下"
                    },
                    {
                        "value": "10-50元",
                        "label": "10-50元"
                    },
                    {
                        "value": "50-100元",
                        "label": "50-100元"
                    },
                    {
                        "value": "100元以上",
                        "label": "100元以上"
                    }
                ],
                "defaultValue": "",
                "required": true,
                "remoteFunc": "func_1609919032000_59161",
                "isTag": true
            },
            "key": "1609919032000_59161",
            "model": "radio_1609919032000_59161",
            "rules": []
        }
    ],
    "config": {
        "labelWidth": 219,
        "labelPosition": "left",
        "size": "small"
    }
}
const demo3 = {
    "list": [{
            "type": "text",
            "name": "",
            "icon": "el-icon-share",
            "options": {
                "nameDisabled": true,
                "defaultValue": "古诗词赏析",
                "position": "center",
                "lineHeight": 25,
                "size": 18,
                "leftPadding": 0,
                "color": "#000000",
                "weight": "700",
                "style": "",
                "remoteFunc": "func_1610000131000_69738"
            },
            "key": "1610000131000_69738",
            "model": "text_1610000131000_69738",
            "rules": []
        },
        {
            "type": "divider",
            "name": "",
            "icon": "el-icon-share",
            "options": {
                "nameDisabled": true,
                "defaultValue": "《静夜思》【唐】李白",
                "position": "center",
                "remoteFunc": "func_1609999313000_12833"
            },
            "key": "1609999313000_12833",
            "model": "divider_1609999313000_12833",
            "rules": []
        },
        {
            "type": "text",
            "name": "",
            "icon": "el-icon-share",
            "options": {
                "nameDisabled": true,
                "defaultValue": "床前明月光",
                "position": "center",
                "lineHeight": 5,
                "size": 14,
                "leftPadding": 0,
                "color": "rgb(0, 73, 213)",
                "weight": "700",
                "style": "",
                "remoteFunc": "func_1609999188000_17367"
            },
            "key": "1609999188000_17367",
            "model": "text_1609999188000_17367",
            "rules": []
        },
        {
            "type": "text",
            "name": "",
            "icon": "el-icon-share",
            "options": {
                "nameDisabled": true,
                "defaultValue": "疑是地上霜",
                "position": "center",
                "lineHeight": 5,
                "size": 14,
                "leftPadding": 0,
                "color": "#0049D5",
                "weight": "700",
                "style": "",
                "remoteFunc": "func_1609999241000_1573"
            },
            "key": "1609999241000_1573",
            "model": "text_1609999241000_1573",
            "rules": []
        },
        {
            "type": "text",
            "name": "",
            "icon": "el-icon-share",
            "options": {
                "nameDisabled": true,
                "defaultValue": "举头望明月",
                "position": "center",
                "lineHeight": 5,
                "size": 14,
                "leftPadding": 0,
                "color": "#0049D5",
                "weight": "700",
                "style": "",
                "remoteFunc": "func_1609999269000_31449"
            },
            "key": "1609999269000_31449",
            "model": "text_1609999269000_31449",
            "rules": []
        },
        {
            "type": "text",
            "name": "",
            "icon": "el-icon-share",
            "options": {
                "nameDisabled": true,
                "defaultValue": "低头思故乡",
                "position": "center",
                "lineHeight": 5,
                "size": 14,
                "leftPadding": 0,
                "color": "#0049D5",
                "weight": "700",
                "style": "",
                "remoteFunc": "func_1609999286000_95900"
            },
            "key": "1609999286000_95900",
            "model": "text_1609999286000_95900",
            "rules": []
        },
        {
            "type": "divider",
            "name": "",
            "icon": "el-icon-share",
            "options": {
                "nameDisabled": true,
                "defaultValue": "《登鹳雀楼》【唐】王之涣",
                "position": "center",
                "remoteFunc": "func_1610000157000_39046"
            },
            "key": "1610000157000_39046",
            "model": "divider_1610000157000_39046",
            "rules": []
        },
        {
            "type": "text",
            "name": "",
            "icon": "el-icon-share",
            "options": {
                "nameDisabled": true,
                "defaultValue": "白日依山尽",
                "position": "center",
                "lineHeight": 5,
                "size": 14,
                "leftPadding": 0,
                "color": "rgb(0, 73, 213)",
                "weight": "700",
                "style": "",
                "remoteFunc": "func_1610000246000_43511"
            },
            "key": "1610000246000_43511",
            "model": "text_1610000246000_43511",
            "rules": []
        },
        {
            "type": "text",
            "name": "",
            "icon": "el-icon-share",
            "options": {
                "nameDisabled": true,
                "defaultValue": "黄河入海流",
                "position": "center",
                "lineHeight": 5,
                "size": 14,
                "leftPadding": 0,
                "color": "rgb(0, 73, 213)",
                "weight": "700",
                "style": "",
                "remoteFunc": "func_1610000247000_68818"
            },
            "key": "1610000247000_68818",
            "model": "text_1610000247000_68818",
            "rules": []
        },
        {
            "type": "text",
            "name": "",
            "icon": "el-icon-share",
            "options": {
                "nameDisabled": true,
                "defaultValue": "欲穷千里目",
                "position": "center",
                "lineHeight": 5,
                "size": 14,
                "leftPadding": 0,
                "color": "#0049D5",
                "weight": "700",
                "style": "",
                "remoteFunc": "func_1610000248000_63087"
            },
            "key": "1610000248000_63087",
            "model": "text_1610000248000_63087",
            "rules": []
        },
        {
            "type": "text",
            "name": "",
            "icon": "el-icon-share",
            "options": {
                "nameDisabled": true,
                "defaultValue": "更上一层楼",
                "position": "center",
                "lineHeight": 5,
                "size": 14,
                "leftPadding": 0,
                "color": "#0049D5",
                "weight": "700",
                "style": "",
                "remoteFunc": "func_1610000249000_50591"
            },
            "key": "1610000249000_50591",
            "model": "text_1610000249000_50591",
            "rules": []
        }
    ],
    "config": {
        "labelWidth": 100,
        "labelPosition": "right",
        "size": "small"
    }
}
const demo4 = {
    "list": [{
            "type": "textarea",
            "name": "商品评价",
            "icon": "el-icon-share",
            "options": {
                "width": "30%",
                "rows": 2,
                "placeholder": "请输入商品评价",
                "defaultValue": "",
                "required": true,
                "maxlength": "",
                "remoteFunc": "func_1610002327000_13347",
                "requiredMsg": "宝贝怎么样，说两句呗"
            },
            "key": "1610002327000_13347",
            "model": "textarea_1610002327000_13347",
            "rules": []
        },
        {
            "type": "rate",
            "name": "描述相符",
            "icon": "el-icon-share",
            "options": {
                "required": true,
                "defaultValue": 0,
                "max": 5,
                "remoteFunc": "func_1610002358000_6129",
                "requiredMsg": "请点亮小星星"
            },
            "key": "1610002358000_6129",
            "model": "rate_1610002358000_6129",
            "rules": []
        },
        {
            "type": "rate",
            "name": "物流服务",
            "icon": "el-icon-share",
            "options": {
                "required": true,
                "defaultValue": 0,
                "max": 5,
                "remoteFunc": "func_1610002395000_19198",
                "requiredMsg": "请点亮小星星"
            },
            "key": "1610002395000_19198",
            "model": "rate_1610002395000_19198",
            "rules": []
        },
        {
            "type": "rate",
            "name": "服务态度",
            "icon": "el-icon-share",
            "options": {
                "required": true,
                "defaultValue": 0,
                "max": 5,
                "remoteFunc": "func_1610002408000_17200",
                "requiredMsg": "请点亮小星星"
            },
            "key": "1610002408000_17200",
            "model": "rate_1610002408000_17200",
            "rules": []
        },
        {
            "type": "radio",
            "name": "是否匿名",
            "icon": "el-icon-share",
            "options": {
                "inline": "inline-block",
                "valueData": [{
                        "value": "否",
                        "label": "否"
                    },
                    {
                        "value": "是",
                        "label": "是"
                    }
                ],
                "defaultValue": "否",
                "required": false,
                "remoteFunc": "func_1610002463000_46492",
                "isTag": true
            },
            "key": "1610002463000_46492",
            "model": "radio_1610002463000_46492",
            "rules": []
        }
    ],
    "config": {
        "labelWidth": 100,
        "labelPosition": "right",
        "size": "small"
    }
}

export {
    demo1,
    demo2,
    demo3,
    demo4
}