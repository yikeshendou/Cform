import Vue from 'vue'
import App from './App.vue'
import store from './store'
import router from "./router"
import ElementUI from 'element-ui';
import 'element-ui/lib/theme-chalk/index.css';
Vue.use(ElementUI,{size:'small'});

Vue.config.productionTip = false


new Vue({
  store,
  router,
  render: function (h) { return h(App) }
}).$mount('#app')
