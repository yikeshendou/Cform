import Vue from 'vue'
import VueRouter from 'vue-router'

import Home from "../components/container.vue"
import Demo from "../components/formsDemos/demo.vue"


Vue.use(VueRouter)


const routes = [{
    path: '/',
    name: '首页',
    component: Home
}, {
    path: "/demo",
    name: "实例表单",
    component: Demo
}]


export default new VueRouter({
    routes
})